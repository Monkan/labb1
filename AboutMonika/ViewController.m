//
//  ViewController.m
//  AboutMonika
//
//  Created by ITHS on 2016-01-28.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *changeColorButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)changeBackgroundColor:(UIButton *)sender {
    self.view.backgroundColor = [UIColor greenColor];
}

@end
